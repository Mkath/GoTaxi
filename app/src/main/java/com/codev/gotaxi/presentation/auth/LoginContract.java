package com.codev.gotaxi.presentation.auth;

import android.support.annotation.NonNull;

import com.codev.gotaxi.core.BasePresenter;
import com.codev.gotaxi.core.BaseView;

/**
 * Created by miguel on 14/06/17.
 */

public interface LoginContract {
    interface View extends BaseView<Presenter>{
        void loginSuccessfully();
    }
    interface Presenter extends BasePresenter{
        void login(@NonNull String email,@NonNull String password);
        void loginWithFb(@NonNull String token);
    }
}
