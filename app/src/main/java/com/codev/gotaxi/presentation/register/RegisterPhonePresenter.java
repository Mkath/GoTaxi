package com.codev.gotaxi.presentation.register;

import android.content.Context;
import android.support.annotation.NonNull;

import com.codev.gotaxi.R;
import com.codev.gotaxi.data.entities.AccessTokenEntity;
import com.codev.gotaxi.data.entities.UserEntity;
import com.codev.gotaxi.data.repositories.local.SessionManager;
import com.codev.gotaxi.data.repositories.remote.ServiceFactory;
import com.codev.gotaxi.data.repositories.remote.request.UserRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by miguel on 14/06/17.
 */

public class RegisterPhonePresenter implements RegisterPhoneContract.Presenter {

    private RegisterPhoneContract.View mView;
    private Context context;
    private SessionManager sessionManager;

    public RegisterPhonePresenter(RegisterPhoneContract.View mView, Context context) {
        this.mView = mView;
        this.context = context;
        sessionManager = new SessionManager(context);
        this.mView.setPresenter(this);
    }

    @Override
    public void registerPhone(@NonNull String phone) {
        mView.setLoadingIndicator(true);
        AccessTokenEntity tokenEntity = sessionManager.getUserToken();
        UserRequest userRequest = ServiceFactory.createService(UserRequest.class);
        Call<UserEntity> call = userRequest.registerPhone(tokenEntity.getAccessToken(),
                tokenEntity.getId(), phone);
        call.enqueue(new Callback<UserEntity>() {
            @Override
            public void onResponse(Call<UserEntity> call, Response<UserEntity> response) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                if(response.isSuccessful()){
                    sessionManager.setUser(response.body());
                    mView.registerPhoneSuccessfully();
                }
                else {
                    mView.showErrorMessage(context.getString(R.string.an_error_occurred_please_try_it_later));
                }
            }

            @Override
            public void onFailure(Call<UserEntity> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage(context.getString(R.string.no_connection_to_server_please_try_it_later));
            }
        });
    }

    @Override
    public void start() {

    }
}
