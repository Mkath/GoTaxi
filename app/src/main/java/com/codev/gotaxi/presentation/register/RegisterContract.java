package com.codev.gotaxi.presentation.register;

import android.support.annotation.NonNull;

import com.codev.gotaxi.core.BasePresenter;
import com.codev.gotaxi.core.BaseView;
import com.codev.gotaxi.data.entities.UserEntity;

/**
 * Created by miguel on 14/06/17.
 */

public interface RegisterContract {
    interface View extends BaseView<Presenter>{
        void registerSuccessfully();
    }
    interface Presenter extends BasePresenter{
        void register(@NonNull UserEntity userEntity);
    }
}
