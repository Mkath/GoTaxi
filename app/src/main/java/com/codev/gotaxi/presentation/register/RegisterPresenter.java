package com.codev.gotaxi.presentation.register;

import android.content.Context;
import android.support.annotation.NonNull;

import com.codev.gotaxi.R;
import com.codev.gotaxi.data.entities.AccessTokenEntity;
import com.codev.gotaxi.data.entities.UserEntity;
import com.codev.gotaxi.data.repositories.local.SessionManager;
import com.codev.gotaxi.data.repositories.remote.ServiceFactory;
import com.codev.gotaxi.data.repositories.remote.request.UserRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by miguel on 14/06/17.
 */

public class RegisterPresenter implements RegisterContract.Presenter {

    private RegisterContract.View mView;
    private Context context;
    private SessionManager sessionManager;

    public RegisterPresenter(RegisterContract.View mView, Context context) {
        this.mView = mView;
        this.context = context;
        sessionManager = new SessionManager(context);
        this.mView.setPresenter(this);
    }

    @Override
    public void register(@NonNull UserEntity userEntity) {
        mView.setLoadingIndicator(true);
        UserRequest userRequest = ServiceFactory.createService(UserRequest.class);
        Call<AccessTokenEntity> call = userRequest.registerUser(userEntity.getFirst_name(),userEntity.getLast_name(),
                userEntity.getEmail(),userEntity.getPassword(),userEntity.getGender(),userEntity.getPhone());
        call.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if(!mView.isActive()){
                    return;
                }
                if (response.isSuccessful()){
                    getAccount(response.body());
                }
                else{
                    mView.setLoadingIndicator(false);
                    mView.showErrorMessage(context.getString(R.string.an_error_occurred_please_try_it_later));
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage(context.getString(R.string.no_connection_to_server_please_try_it_later));
            }
        });
    }

    private void getAccount(final AccessTokenEntity accessTokenEntity){
        UserRequest userRequest = ServiceFactory.createService(UserRequest.class);
        Call<UserEntity> call = userRequest.getAccount(accessTokenEntity.getAccessToken(),accessTokenEntity.getId());
        call.enqueue(new Callback<UserEntity>() {
            @Override
            public void onResponse(Call<UserEntity> call, Response<UserEntity> response) {
                if(!mView.isActive()){
                    return;
                }
                if(response.isSuccessful()){
                    openSession(accessTokenEntity,response.body());
                }
                else{
                    mView.setLoadingIndicator(false);
                    mView.showErrorMessage(context.getString(R.string.an_error_occurred_please_try_it_later));
                }
            }

            @Override
            public void onFailure(Call<UserEntity> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage(context.getString(R.string.no_connection_to_server_please_try_it_later));
            }
        });
    }

    private void openSession(AccessTokenEntity accessTokenEntity,UserEntity userEntity){
        mView.setLoadingIndicator(false);
        sessionManager.openSession(accessTokenEntity,userEntity);
        mView.registerSuccessfully();
    }

    @Override
    public void start() {

    }
}
