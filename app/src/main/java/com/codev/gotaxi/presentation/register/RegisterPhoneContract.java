package com.codev.gotaxi.presentation.register;

import android.support.annotation.NonNull;

import com.codev.gotaxi.core.BasePresenter;
import com.codev.gotaxi.core.BaseView;

/**
 * Created by miguel on 14/06/17.
 */

public interface RegisterPhoneContract {
    interface View extends BaseView<Presenter>{
        void registerPhoneSuccessfully();
    }
    interface Presenter extends BasePresenter{
        void registerPhone(@NonNull String phone);
    }
}
