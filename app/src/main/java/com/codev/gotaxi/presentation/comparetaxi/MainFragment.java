package com.codev.gotaxi.presentation.comparetaxi;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codev.gotaxi.R;
import com.codev.gotaxi.core.BaseActivity;
import com.codev.gotaxi.core.BaseFragment;
import com.codev.gotaxi.data.entities.CabifyEntity;
import com.codev.gotaxi.data.entities.CarEntity;
import com.codev.gotaxi.data.entities.UberEntity;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.TravelMode;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_CANCELED;


/**
 * Created by kath on 20/12/17.
 */

public class MainFragment extends BaseFragment implements
        OnMapReadyCallback, LocationListener, CompareTaxiContract.View, FragmentInterface {
    private static final String TAG = CompareTaxiActivity.class.getSimpleName();

    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 100;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final String LOG_TAG = "PlaceSelectionListener";

    double latitude, longitude;
    @BindView(R.id.tv_conectado)
    TextView tvConectado;
    @BindView(R.id.container_toolbar)
    RelativeLayout containerToolbar;
    @BindView(R.id.tv_ubication)
    TextView tvUbication;
    @BindView(R.id.im_ubication)
    ImageView imUbication;
    @BindView(R.id.container_ubicacion_actual)
    LinearLayout containerUbicacionActual;
    @BindView(R.id.tv_a_donde_vas)
    TextView tvADondeVas;
    @BindView(R.id.im_search)
    ImageView imSearch;
    @BindView(R.id.container_destino)
    LinearLayout containerDestino;
    @BindView(R.id.image_center)
    ImageView imageCenter;
    @BindView(R.id.btn_siguiente)
    Button btnSiguiente;
    @BindView(R.id.layout)
    RelativeLayout layout;
    Unbinder unbinder;
    @BindView(R.id.btn_open_uber)
    Button btnOpenUber;
    @BindView(R.id.btn_open_cabify)
    Button btnOpenCabify;
    @BindView(R.id.list_car)
    RecyclerView listCar;
    @BindView(R.id.container_list)
    LinearLayout containerList;
    @BindView(R.id.fl_progress_bar)
    FrameLayout flProgressBar;

    private boolean isFirst = true;
    SupportMapFragment mapFragment;
    private AlertDialog dialogogps, dialognetwork;
    private LocationManager mlocManager;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private GoogleMap mMap;
    LatLng center;
    Geocoder geocoder;
    private boolean first = true;

    private String direccionActual, direccionDestino;
    double sendLatitude, sendLongitude, sendLatitudeDestino, sendLongitudeDestino;

    private LinearLayoutManager mLayoutManager;
    private CompareTaxiContract.Presenter mPresenter;

    private int ubication = 1;

    private Marker startMarker, finalMarker;
    private Handler mHandler = new Handler();
    private static final int overview = 0;

    private MainInterface mainInterface;
    private boolean firstLoading = true;


    private LinearLayoutManager mLinearLayoutManager;
    private CarAdapter carAdapter;

    private ArrayList<CarEntity> uberList, cabifyList;

    public MainFragment() {
        // Requires empty public constructor
    }

    public static MainFragment newInstance() {

        return new MainFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            locationStart();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new CompareTaxiPresenter(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_map, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
 /*       mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvList.setLayoutManager(mLayoutManager);
        mAdapter = new MainAdapter(new ArrayList<MovilResponse>(), getContext(), (mainItem) mPresenter);
        rvList.setAdapter(mAdapter);*/

        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        listCar.setLayoutManager(mLinearLayoutManager);
        carAdapter = new CarAdapter(new ArrayList<CarEntity>());
        listCar.setAdapter(carAdapter);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment == null) {
            FragmentManager fr = getFragmentManager();
            FragmentTransaction ft = fr.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            ft.replace(R.id.map, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);

        //tvUbication.setText(direccionActual);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        // Rest of the stuff you need to do with the map
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setMyLocationButtonEnabled(false);
        uiSettings.setCompassEnabled(false);

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //locationStart();
            } else {
                checkLocationPermission();
            }
        }

        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                // Toast.makeText(getContext(), "HOLIÇ", Toast.LENGTH_SHORT).show();
            }
        });

        mMap.setMyLocationEnabled(true);
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                if (getContext() != null) {
                    mLastLocation = location;
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    if (first) {
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
                        first = false;
                    }
                    getDireccion();
                }
            }
        }
    };

    public void getDireccion() {
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                center = mMap.getCameraPosition().target;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    List<Address> addresses = geocoder.getFromLocation(
                            center.latitude, center.longitude, 1);
                    if (addresses.size() > 0) {
                        Address address = addresses.get(0);

                        StringBuilder sb = new StringBuilder(128);
                        sb.append(" ");
                        sb.append(address.getAddressLine(0));
                        if (addresses.size() > 2) {
                            sb.append(", ");
                            sb.append(address.getAddressLine(2));
                        }

                        if (ubication == 1) {
                            sendLatitude = center.latitude;
                            sendLongitude = center.longitude;
                            direccionActual = sb.toString();
                            tvUbication.setText(sb.toString());
                        } else {
                            sendLatitudeDestino = center.latitude;
                            sendLongitudeDestino = center.longitude;
                            direccionDestino = sb.toString();
                            tvADondeVas.setText(sb.toString());
                        }
                    }
                } catch (Exception ex) {
                }
            }
        });

    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Habilitar GPS")
                        .setMessage("Se requieren permisos de gps")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

    private void locationStart() {
        mlocManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnabled) {
            alertafalta();
        }
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onPause() {
        super.onPause();
        if (dialogogps != null) {
            dialogogps.dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        super.onPause();
        if (dialogogps != null) {
            dialogogps.dismiss();
        }
        unbinder.unbind();
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        switch (i) {
            case LocationProvider.AVAILABLE:
                Log.d("debug", "LocationProvider.AVAILABLE");
                break;
            case LocationProvider.OUT_OF_SERVICE:
                Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                break;
            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                break;
        }
    }

    @Override
    public void onProviderEnabled(String s) {
        //Toast.makeText(getContext(), "GPS ACTIVADO", Toast.LENGTH_SHORT).show();
        if (dialogogps != null) {
            dialogogps.dismiss();
        }
        if (dialognetwork != null) {
            dialognetwork.dismiss();
        }
    }

    @Override
    public void onProviderDisabled(String s) {
        if (s.equals("gps")) {
            alertafalta();
        }
      /*  if(s.equals("network")) {
            alertafaltaNetwork();}
            */
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(getContext(), "Please provide the permission", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    public void alertafalta() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Advertencia");
        builder.setMessage("El sistema GPS esta desactivado, para continuar presione el boton activar?");
        builder.setCancelable(false);
        builder.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                startActivity(new Intent
                        (Settings.ACTION_LOCATION_SOURCE_SETTINGS));

            }
        });
        dialogogps = builder.create();
        dialogogps.show();
    }

    public void alertafaltaNetwork() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Advertencia");
        builder.setMessage("El Datos desactivados, para continuar presione el boton activar?");
        builder.setCancelable(false);
        builder.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                startActivity(new Intent
                        (Settings.ACTION_NETWORK_OPERATOR_SETTINGS));

            }
        });
        dialognetwork = builder.create();
        dialognetwork.show();
    }

    @OnClick({R.id.container_ubicacion_actual, R.id.container_destino, R.id.tv_ubication,
            R.id.tv_a_donde_vas, R.id.btn_siguiente, R.id.im_ubication, R.id.btn_open_uber, R.id.btn_open_cabify})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_ubication:
                ubication = 1;
                openPlaceAutoCompleteView();
                break;
            case R.id.im_ubication:
                mMap.clear();
                //Place current location marker
                LatLng latLng = new LatLng(latitude, longitude);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(18));
                break;

            case R.id.container_ubicacion_actual:
                ubication = 1;
                openPlaceAutoCompleteView();
                break;
            case R.id.container_destino:
                ubication = 2;
                openPlaceAutoCompleteView();
                break;
            case R.id.tv_a_donde_vas:
                ubication = 2;
                openPlaceAutoCompleteView();
                break;
            case R.id.btn_siguiente:
                if (ubication == 1) {
                    uiDestino();

                } else {
                    if (ubication == 2) {
                        uiRuta();
                        mPresenter.compareTaxi(sendLatitude, sendLongitude, sendLatitudeDestino, sendLongitudeDestino);
                    }
                }
                break;
            case R.id.btn_open_uber:
                carAdapter.setItems(uberList, getContext());
                btnOpenUber.setBackground(getResources().getDrawable(R.drawable.back_round_button_car));
                btnOpenCabify.setBackground(getResources().getDrawable(R.drawable.back_round_border_white));
                break;

            case R.id.btn_open_cabify:
                btnOpenUber.setBackground(getResources().getDrawable(R.drawable.back_round_border_white));
                carAdapter.setItems(cabifyList, getContext());
                btnOpenCabify.setBackground(getResources().getDrawable(R.drawable.back_round_button_car));

                break;
           /* case R.id.btn_location:
                mMap.clear();
                //Place current location marker
                LatLng latLng2 = new LatLng(-12.022685921344909, -77.10744604468346);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng2));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                Toast.makeText(getContext(), "Recogerme del Aeropuerto", Toast.LENGTH_SHORT).show();
                break;*/
        }
    }

    public void uiDestino() {
        ubication = 2;
        mainInterface.showDisplayHome(true);
        tvConectado.setText("¿A dónde vas?");
        containerUbicacionActual.setClickable(false);
        tvUbication.setEnabled(false);
        containerDestino.setVisibility(View.VISIBLE);
        btnSiguiente.setText("Consultar");
    }

    public void uiRuta() {
        ubication = 3;
        tvConectado.setVisibility(View.GONE);
        containerDestino.setVisibility(View.GONE);
        containerUbicacionActual.setVisibility(View.GONE);
        containerDestino.setVisibility(View.GONE);
        imageCenter.setVisibility(View.GONE);
        updateUIjourney();
        //sendInfo();
    }

    private DirectionsResult getDirectionsDetails(String origin, String destination, TravelMode mode) {
        DateTime now = new DateTime();
        try {
            return DirectionsApi.newRequest(getGeoContext())
                    .mode(mode)
                    .origin(origin)
                    .destination(destination)
                    .departureTime(now)
                    .await();
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void updateUIjourney() {

        DirectionsResult results = obtenerResults();
        if (results != null) {
            marcarRuta(results, mMap);
        } else {


            mHandler.postDelayed(new Runnable() {


                @Override
                public void run() {
                    final DirectionsResult results2 = obtenerResults();
                    final GoogleMap googleMap2 = mMap;
                    marcarRuta(results2, googleMap2);
                }
            }, 3000);

        }

    }

    private DirectionsResult obtenerResults() {
        DirectionsResult result = getDirectionsDetails(direccionActual, direccionDestino, TravelMode.DRIVING);
        return result;
    }

    private void marcarRuta(DirectionsResult results, GoogleMap googleMap) {
        if (results != null) {
            addPolyline(results, googleMap);
            positionCamera(results.routes[overview], googleMap);
            addMarkersToMap(results, googleMap);
        }

    }

    private void addMarkersToMap(DirectionsResult results, GoogleMap mMap) {
        startMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(results.routes[overview].legs[overview].startLocation.lat,
                results.routes[overview].legs[overview].startLocation.lng)).title(results.routes[overview].legs[overview].startAddress)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_position_start)));
        finalMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(results.routes[overview].legs[overview].endLocation.lat,
                results.routes[overview].legs[overview].endLocation.lng)).title(results.routes[overview].legs[overview].endAddress)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_position_end))
                .snippet(getEndLocationTitle(results)));

        finalMarker.showInfoWindow();


        updateCameraView();

    }

    private void updateCameraView() {
        double lat = 0;
        double lon = 0;
        if (startMarker != null) {
            lat = lat + startMarker.getPosition().latitude;
            lon = lon + startMarker.getPosition().longitude;
        }
        if (finalMarker != null) {
            lat = (lat + finalMarker.getPosition().latitude) / 2;
            lon = (lon + finalMarker.getPosition().longitude) / 2;

        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 12));
    }

    private void positionCamera(DirectionsRoute route, GoogleMap mMap) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(route.legs[overview].startLocation.lat, route.legs[overview].startLocation.lng), 10));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
    }

    private void addPolyline(DirectionsResult results, GoogleMap mMap) {
        List<LatLng> decodedPath = PolyUtil.decode(results.routes[overview].overviewPolyline.getEncodedPath());
        mMap.addPolyline(new PolylineOptions().addAll(decodedPath).color(getResources().getColor(R.color.colorPrimary)));
    }

    private String getEndLocationTitle(DirectionsResult results) {
        return "Tiempo :" + results.routes[overview].legs[overview].duration.humanReadable + " Distancia :" + results.routes[overview].legs[overview].distance.humanReadable;
    }

    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext
                .setQueryRateLimit(3)
                .setApiKey(getString(R.string.google_map_key))
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // super.onActivityResult(requestCode, resultCode, data);
        // Add your code here
        if (resultCode == Activity.RESULT_OK) {
            Place place = PlaceAutocomplete.getPlace(getActivity(), data);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15));
            if (ubication == 1) {
                //startPlace = place;
                sendLatitude = place.getLatLng().latitude;
                sendLongitude = place.getLatLng().longitude;
                tvUbication.setText(place.getAddress());
            } else {
                //endPlace = place;
                sendLatitudeDestino = place.getLatLng().latitude;
                sendLongitudeDestino = place.getLatLng().longitude;
                tvADondeVas.setText(place.getAddress());
            }
            //onPlaceSelected(place);

        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(getActivity(), data);
            Toast.makeText(getContext(), "Error " + status, Toast.LENGTH_SHORT).show();

        } else if (resultCode == RESULT_CANCELED) {
            // The user canceled the operation.
        }
    }

    public void openPlaceAutoCompleteView() {

        try {
            AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                    .setCountry("PE")
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                    .build();
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .setFilter(autocompleteFilter)
                            .build(getActivity());
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(CompareTaxiContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {

        flProgressBar.setVisibility(active?View.VISIBLE:View.GONE);

    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);

    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dialogogps != null) {
            dialogogps.dismiss();
        }
    }

    @Override
    public void getUberPrices(ArrayList<UberEntity> list) {

        btnSiguiente.setVisibility(View.GONE);
        containerList.setVisibility(View.VISIBLE);

        uberList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            CarEntity carEntity = new CarEntity();
            double number = (list.get(i).getHigh_estimate() + list.get(i).getLow_estimate()) / 2;
            carEntity.setDistance(list.get(i).getDistance());
            carEntity.setDuration(list.get(i).getDuration());
            carEntity.setEstimate(list.get(i).getCurrency_code() + " " + number);
            carEntity.setServiceName(list.get(i).getDisplay_name());
            carEntity.setTypeCar(1);
            uberList.add(carEntity);
        }
        carAdapter.setItems(uberList, getContext());


        //       Toast.makeText(getContext(), list.get(0).getDisplay_name() + list.get(0).getLow_estimate() + " ", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getCabifyPrices(ArrayList<CabifyEntity> list) {
        //  Toast.makeText(getContext(), list.get(0).getVehicle_type().getName() + list.get(0).getTotal_price() + " ", Toast.LENGTH_SHORT).show();
        cabifyList = new ArrayList<>();

        if(list.size()>1){
            for (int i = 0; i < 5; i++) {
                CarEntity carEntity = new CarEntity();
                carEntity.setEstimate(String.valueOf(list.get(i).getFormatted_price()));
                carEntity.setServiceName(list.get(i).getVehicle_type().getName());
                carEntity.setImagerCar(list.get(i).getVehicle_type().getIcons().getRegular());
                carEntity.setTypeCar(2);
                cabifyList.add(carEntity);
            }
        } else{
            for (int i = 0; i < 1; i++) {
                CarEntity carEntity = new CarEntity();
                carEntity.setEstimate(String.valueOf(list.get(i).getFormatted_price()));
                carEntity.setServiceName(list.get(i).getVehicle_type().getName());
                carEntity.setImagerCar(list.get(i).getVehicle_type().getIcons().getRegular());
                carEntity.setTypeCar(2);
                cabifyList.add(carEntity);
            }
        }



    }

    @Override
    public void updateUi() {

        ubication = 1;
        containerDestino.setVisibility(View.GONE);
        tvUbication.setEnabled(true);
        containerUbicacionActual.setVisibility(View.VISIBLE);
        containerUbicacionActual.setClickable(true);
        btnSiguiente.setText("SEGUIR");
        tvConectado.setText("¿Dónde Estoy?");
        firstLoading = true;
        tvConectado.setVisibility(View.VISIBLE);
        containerList.setVisibility(View.GONE);
        mMap.clear();
        btnOpenUber.setBackground(getResources().getDrawable(R.drawable.back_round_button_car));
        btnOpenCabify.setBackground(getResources().getDrawable(R.drawable.back_round_border_white));
        btnSiguiente.setVisibility(View.VISIBLE);
        imageCenter.setVisibility(View.VISIBLE);
        LatLng latLng2 = new LatLng(latitude, longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng2));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mainInterface = (MainInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement MyInterface");
        }
    }
}
