package com.codev.gotaxi.data.entities;

import java.io.Serializable;

/**
 * Created by miguel on 04/08/16.
 */
public class AccessTokenEntity implements Serializable {
    private String id;
    private String userId;

    public void setId(String id) {
        this.id = id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return id;
    }

    public String getId() {
        return userId;
    }
}
