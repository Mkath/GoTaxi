package com.codev.gotaxi.data.repositories.remote.request;

import com.codev.gotaxi.data.entities.CabifyBodyEntity;
import com.codev.gotaxi.data.entities.CabifyEntity;
import com.codev.gotaxi.data.entities.PricesUberEntity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by miguel on 31/05/17.
 */

public interface CompareTaxiRequest {
    @POST("v2/estimate")
    Call<ArrayList<CabifyEntity>> getCabifyPrices(@Header("Authorization") String token, @Header("Accept-Language") String language, @Body CabifyBodyEntity cabifyBodyEntity);

    @GET("estimates/price")
    Call<PricesUberEntity> getUberPrices(@Header("Authorization") String token, @Query("start_latitude") double start_latitude,
                                         @Query("start_longitude") double start_longitude, @Query("end_latitude") double end_latitude,
                                         @Query("end_longitude") double end_longitude);
}
