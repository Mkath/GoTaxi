package com.codev.gotaxi.data.entities;

/**
 * Created by kath on 1/05/18.
 */

public class CarEntity {
    private String serviceName;
    private double distance;
    private double duration;
    private String estimate;
    private double lowEstimate;
    private double highEstimate;
    private String imagerCar;

    private int typeCar;

    public int getTypeCar() {
        return typeCar;
    }

    public void setTypeCar(int typeCar) {
        this.typeCar = typeCar;
    }

    public String getImagerCar() {
        return imagerCar;
    }

    public void setImagerCar(String imagerCar) {
        this.imagerCar = imagerCar;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public String getEstimate() {
        return estimate;
    }

    public void setEstimate(String estimate) {
        this.estimate = estimate;
    }

    public double getLowEstimate() {
        return lowEstimate;
    }

    public void setLowEstimate(double lowEstimate) {
        this.lowEstimate = lowEstimate;
    }

    public double getHighEstimate() {
        return highEstimate;
    }

    public void setHighEstimate(double highEstimate) {
        this.highEstimate = highEstimate;
    }
}
