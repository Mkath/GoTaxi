package com.codev.gotaxi.data.repositories.remote.request;

import com.codev.gotaxi.data.entities.AccessTokenEntity;
import com.codev.gotaxi.data.entities.UserEntity;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by miguel on 14/06/17.
 */

public interface UserRequest {
    @FormUrlEncoded
    @POST("users/login")
    Call<AccessTokenEntity> login(@Field("email") String email,
                                  @Field("password") String password);

    @FormUrlEncoded
    @POST("users/loginfb")
    Call<AccessTokenEntity> loginFb(@Field("accessToken") String token);

    @GET("users/{id}")
    Call<UserEntity> getAccount(@Header("Authorization") String token, @Path("id") String id);

    @FormUrlEncoded
    @POST("users")
    Call<AccessTokenEntity> registerUser(@Field("first_name") String first_name,@Field("last_name") String last_name,
                                         @Field("email") String email,@Field("password") String password,
                                         @Field("gender") String gender,@Field("cellphone") String cellphone);

    @FormUrlEncoded
    @PATCH("users/{id}")
    Call<UserEntity> registerPhone(@Header("Authorization") String token,@Path("id") String id,
                                    @Field("cellphone") String cellphone);

    @FormUrlEncoded
    @PATCH("users/{id}")
    Call<UserEntity> updateUser(@Header("Authorization") String token, @Path("id") String id,
                                @Field("first_name") String first_name,@Field("last_name") String last_name,
                                @Field("email") String email,@Field("gender") String gender,
                                @Field("cellphone") String cellphone);

}
